﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Chinook.MeteoStation;
using Newtonsoft.Json;

namespace MeteoStationWorker
{
    public class WindController : ApiController
    {
        #region ===== Wind speed meteres =====
        private readonly static Dictionary<int, WindSpeedMeter> WindSpeedMeters = new Dictionary<int, WindSpeedMeter>
        {
            {1, CreateWindSpeedMeter(1.0)},
            {2,CreateWindSpeedMeter(2.0)}
            // TODO add more wind speed meters here...
        };
        static WindSpeedMeter CreateWindSpeedMeter(double baseSpeed)
        {
            return new WindSpeedMeter
            {
                BaseSpeed = baseSpeed
            };
        }

        #endregion

        #region ===== Wind direction meteres =====
        private readonly static Dictionary<int, WindDirectionMeter> WindDirectionMeters = new Dictionary<int, WindDirectionMeter>
        {
            {1, CreateWindDirectiondMeter(25)},
            {2, CreateWindDirectiondMeter(189)}
            // TODO add more wind direction meters here...
        };
        private static WindDirectionMeter CreateWindDirectiondMeter(double angle)
        {
            return new WindDirectionMeter
            {
                BaseAngle = angle
            };
        }

        #endregion

        #region ===== API =====
        [HttpGet]
        public HttpResponseMessage Speed(int stationId, int sampleSize)
        {
            var windmeter = GetWindmeter(stationId);
            var results = windmeter.Item1.MeasureMany(sampleSize);
            var responseContent = JsonConvert.SerializeObject(results);
            return new HttpResponseMessage
            {
                Content = new StringContent(responseContent),
                StatusCode = HttpStatusCode.OK
            };
        }

        [HttpGet]
        public HttpResponseMessage Direction(int stationId, int sampleSize)
        {
            var windmeter = GetWindmeter(stationId);
            var results = windmeter.Item2.MeasureMany(sampleSize);
            var responseContent = JsonConvert.SerializeObject(results);
            return new HttpResponseMessage
            {
                Content = new StringContent(responseContent),
                StatusCode = HttpStatusCode.OK
            };
        }

        #endregion

        #region ===== Helpers =====
        private static Tuple<WindSpeedMeter, WindDirectionMeter> GetWindmeter(int stationId)
        {
            var speedMeter = WindSpeedMeters[stationId];
            var directionMeter = WindDirectionMeters[stationId];
            var windMeter = new Tuple<WindSpeedMeter, WindDirectionMeter>(speedMeter, directionMeter);
            return windMeter;
        }

        #endregion
    }
}
