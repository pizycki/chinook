using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace MeteoStationWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        private IDisposable _app = null;

        public override void Run()
        {
            Trace.TraceInformation("MeteoStationWorker is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            var endpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["MeteoStationEndpoint"];
            string baseUri = String.Format("{0}://{1}",
                endpoint.Protocol, endpoint.IPEndpoint);
            
            Trace.TraceInformation(String.Format("Starting WebAPI at {0}", baseUri),
                "Information");

            _app = WebApp.Start<Startup>(new StartOptions(url: baseUri));

            bool result = base.OnStart();

            Trace.TraceInformation("MeteoStationWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("MeteoStationWorker is stopping");

            cancellationTokenSource.Cancel();
            runCompleteEvent.WaitOne();

            if (_app != null)
            {
                _app.Dispose();
            }

            base.OnStop();

            Trace.TraceInformation("MeteoStationWorker has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                while (true)
                {
                    Thread.Sleep(10000);
                    Trace.TraceInformation("Working", "Information");
                }
            }
        }
    }
}
