﻿using System.Collections.Generic;
using System.Configuration;
using Chinook.Common.Contracts;
using Chinook.MeteoStation.Proxy;

namespace Chinook.WebSrvWorker
{
    public class WindMeterServiceFactory
    {
        private readonly IEnumerable<IWindMeterDevice> _subscribedWindMeterDevices;
        private readonly string _webServiceAddress = ConfigurationManager.ConnectionStrings["MeteoStationWs"].ConnectionString;

        public WindMeterServiceFactory()
        {
            _subscribedWindMeterDevices = CreateWindMeterDevices();
        }

        private IEnumerable<IWindMeterDevice> CreateWindMeterDevices()
        {
            return new List<IWindMeterDevice>
            {
                new MeteoStationProxy (1, _webServiceAddress),
                new MeteoStationProxy (2, _webServiceAddress)
            };
        }

        public IWindMeterService Create()
        {
            return new Domain.Fsharp.Services.Windmeter(10, _subscribedWindMeterDevices);
        }
    }
}
