﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Chinook.Common.Contracts;
using Newtonsoft.Json;

namespace Chinook.WebSrvWorker
{
    public class MeteoController : ApiController
    {
        private readonly IWindMeterService _windMeterSrv;

        public MeteoController()
        {
            _windMeterSrv = new WindMeterServiceFactory().Create();
        }

        [HttpGet]
        public HttpResponseMessage Wind(string id)
        {
            if (string.IsNullOrEmpty(id))
                return new HttpResponseMessage { StatusCode = HttpStatusCode.NotFound };

            // Parse city IDs
            var cityIds = id.Split(',').Select(int.Parse);

            // Invoke method on service
            var results = _windMeterSrv.GetWindForCities(cityIds);

            // Prepare response
            var responseJson = JsonConvert.SerializeObject(results);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(responseJson)
            };
        }
    }
}
