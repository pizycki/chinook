﻿using System.Collections.Generic;
using System.Linq;
using Chinook.Common.Contracts;
using Moq;
using NUnit.Framework;

namespace Chinook.Domain.Tests
{
    [TestFixture]
    public class MapReduceWindSpeedTests
    {
        private IWindMeterService _windMeterSrv;

        [SetUp]
        public void PrepareWindMeterService()
        {
            var windMeterDevices = GetWindMeterDevices();
            _windMeterSrv = new Fsharp.Services.Windmeter(default(int), windMeterDevices);
        }

        [TearDown]
        public void NullWindMeterService()
        {
            _windMeterSrv = null;
        }

        private IEnumerable<IWindMeterDevice> GetWindMeterDevices()
        {
            var windMeterDeviceMock1 = GetIWindMeterDeviceMock(1,
                new List<double> { 1.0, 1.4, 1.6, 1.1, 1.2, 1.5, 1.3, 1.7, 1.8, 1.9 }, //  sum = 14.5
                new List<double> { 180, 175, 185, 181, 179, 120, 240, 178, 182, 180, 180 }); // sum = 1980

            var windMeterDeviceMock2 = GetIWindMeterDeviceMock(2,
                new List<double> { 2.0, 2.4, 2.6, 2.1, 2.2, 0, 2.3, 2.7, 2.8, 2.9 }, //  sum = 22
                new List<double> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }); // sum = 45

            return new List<IWindMeterDevice> { windMeterDeviceMock1.Object, windMeterDeviceMock2.Object };
        }

        private Mock<IWindMeterDevice> GetIWindMeterDeviceMock(int id, IEnumerable<double> returnedWindSpeeds, IEnumerable<double> returnedWindDirections)
        {
            var mock = new Mock<IWindMeterDevice>();
            mock.SetupGet(x => x.Id).Returns(id);

            // Wind speeds
            mock
                .Setup(x => x.MeasureWindSpeed(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(() => new List<double>(returnedWindSpeeds));

            // Wind directions 
            mock.Setup(x => x.MeasureWindDirection(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(() => new List<double>(returnedWindDirections));

            return mock;
        }

        #region [ Tests ]

        [Test]
        public void MapWinds_AndExpect_MeasurmentsFromTwoStations()
        {
            // Act
            var results = _windMeterSrv.GetWindForCities(new[] { 1, 2 }).ToList();

            // Assert
            Assert.AreEqual(2, results.Count());
        }

        [Test]
        public void MapWinds_AndExpect_FirstToBe1and45()
        {
            // Act
            var results = _windMeterSrv.GetWindForCities(new[] { 1, 2 }).ToList();

            // Assert
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            Assert.AreEqual(1, results.Count(x => x.Speed == 1.45));
        }

        [Test]
        public void MapWinds_AndExpect_SecondToBe2and3875()
        {
            // Act
            var results = _windMeterSrv.GetWindForCities(new[] { 1, 2 }).ToList();

            // Assert
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            Assert.AreEqual(1, results.Count(x => x.Speed == 2.3875));
        }

        #endregion
    }
}
