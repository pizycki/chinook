﻿namespace Chinook.Domain.Fsharp.Services

open Chinook.Common.Contracts
open FSharp.Collections.ParallelSeq
open System.Collections.Generic

type WindMeasurments = {
    StationId : int
    WindSpeed : float
    WindDirection : float
}


type Windmeter(sampleSize : int, windMeterStations : IEnumerable<IWindMeterDevice>) = 
    member this.SampleSize = sampleSize
    member this.WindMeterStations = windMeterStations
    member this.GetWindMeterStations(ids : int seq) : IWindMeterDevice seq = 
        this.WindMeterStations |> Seq.filter (fun s -> Seq.exists (fun id -> s.Id = id) ids)
    
    member this.GetWindMeasurments(ids : int list, cityStations : seq<IWindMeterDevice>) : seq<WindMeasurments> = 
        // Map
        let windSpeeds = this.CollectWindSpeedsFromStations(cityStations, this.SampleSize)
        let windDirections = this.CollectWindDirectionsFromStations(cityStations, this.SampleSize)
        let avgWindSpeed = this.AnalyzeMeasurments windSpeeds |> Map.ofSeq
        let avgWindDirection = this.AnalyzeMeasurments windDirections |> Map.ofSeq
        let measurments = this.combineMeasurments(avgWindSpeed, avgWindDirection)
        measurments
         
    // Combine two dictionaries with repeating Keys
    member this.combineMeasurments(m1 : Map<int, float>, m2 : Map<int, float>) : seq<WindMeasurments> = 
        m1 |> Seq.map (fun p -> { StationId = p.Key; WindSpeed = p.Value; WindDirection = Map.find p.Key m2 } )

    // Collect batch of wind speed measurments with specified sample size
    member this.CollectWindSpeedsFromStations(stations : IWindMeterDevice seq, sampleSize : int) = 
        stations |> PSeq.map (fun x -> x.Id, (x.MeasureWindSpeed (x.Id, sampleSize) |> List.ofSeq))
    
   // Collect batch of wind direction measurments with specified sample size
    member this.CollectWindDirectionsFromStations(stations : IWindMeterDevice seq, sampleSize : int) = 
        stations |> PSeq.map (fun x -> x.Id, (x.MeasureWindDirection (x.Id, sampleSize) |> List.ofSeq))

    // Sort and exclude min and max value, then take avarage of wind speeds.
    member this.AnalyzeMeasurments(ls : (int * float list) seq) = 
        ls |> PSeq.map (fun x -> 
                  fst x,
                  snd x
                  |> Seq.sort
                  |> Common.skipFirstAndLast
                  |> Seq.average)

    interface IWindMeterService with
        member this.GetWindForCities(ids : IEnumerable<int>) = 
            let ids_list = ids |> List.ofSeq
            let cityStations = this.GetWindMeterStations ids
            let results = this.GetWindMeasurments(ids_list, cityStations)
            // Map to transfer object
            results |> Seq.map (fun x -> new Chinook.Common.Models.WindMeasurement(StationId = x.StationId, Speed = x.WindSpeed, Direction = x.WindDirection ))