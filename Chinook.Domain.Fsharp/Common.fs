﻿module Common

    let skipFirstAndLast ls = 
        ls
        |> List.ofSeq
        |> List.tail
        |> List.rev
        |> List.tail
        |> List.rev

            
    // Converts Map to Dictionary
    let mapToDict map = 
        let dict = new System.Collections.Generic.Dictionary<int, double>()
        Seq.iter (fun x -> dict.Add x) map
        dict

    let convertDictToMap dict = 
        (dict :> seq<_>)
        |> Seq.map (|KeyValue|)
        |> Map.ofSeq