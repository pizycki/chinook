using System.Diagnostics;

namespace Chinook.MeteoStation
{
    public class WindSpeedMeter : RandomMeter
    {
        public WindSpeedMeter()
            : base()
        {
            this.BaseSpeed = Random.Next(0, 5) + Random.NextDouble();
            Trace.WriteLine(string.Format("WindSpeedMeter: wind base speed = {0}", BaseSpeed.ToString()));
        }

        public override double Measure()
        {
            return BaseSpeed + Random.NextDouble();
        }

        public double BaseSpeed { get; set; }
    }
}