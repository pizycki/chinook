﻿using System.Diagnostics;

namespace Chinook.MeteoStation
{
    public class WindDirectionMeter : RandomMeter
    {
        public WindDirectionMeter() 
            : base()
        {
            do
            {
                BaseAngle = Random.Next(0, 360) + Random.NextDouble();
            } while (BaseAngle < 0 || BaseAngle > 360);
            Trace.WriteLine(string.Format("WindDirectionMeter: wind direction angle = {0}", BaseAngle.ToString()));
        }

        public override double Measure()
        {
            return BaseAngle + Random.NextDouble();
        }

        public double BaseAngle { get; set; }
    }
}
