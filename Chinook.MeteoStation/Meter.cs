﻿using System;

namespace Chinook.MeteoStation
{
    public abstract class RandomMeter
    {
        protected static readonly Random Random = new Random();

        public abstract double Measure();

        public virtual double[] MeasureMany(int sampleSize)
        {
            var arr = new double[sampleSize];
            for (int i = 0; i < sampleSize; i++)
                arr[i] = Measure();

            return arr;
        }
    }
}
