﻿using System.Collections.Generic;
using System.Net;
using Chinook.Common.Contracts;
using Newtonsoft.Json;
using RestSharp;

namespace Chinook.MeteoStation.Proxy
{
    public class MeteoStationProxy : IWindMeterDevice
    {
        public int Id { get; private set; }
        public string Address { get; private set; }

        public MeteoStationProxy(int id, string address)
        {
            this.Address = address;
            this.Id = id;
        }

        public IEnumerable<double> MeasureWindSpeed(int stationId, int sampleSize)
        {
            var wsResponse = InvokeWebService("Wind/Speed",
                new Dictionary<string, object>
                {
                    {"stationId", stationId},
                    {"sampleSize", sampleSize}
                });

            if (wsResponse == null) return null;

            var windSpeeds = JsonConvert.DeserializeObject(wsResponse.Content, typeof(List<double>)) as IEnumerable<double>;
            return windSpeeds;
        }

        public IEnumerable<double> MeasureWindDirection(int stationId, int sampleSize)
        {
            var wsResponse = InvokeWebService("Wind/Direction",
                new Dictionary<string, object>
                {
                    {"stationId", stationId},
                    {"sampleSize", sampleSize}
                });

            if (wsResponse == null) return null;

            var windDirections = JsonConvert.DeserializeObject(wsResponse.Content, typeof(List<double>)) as IEnumerable<double>;
            return windDirections;
        }

        private IRestResponse InvokeWebService(string wsUri, IDictionary<string, object> parameters)
        {
            var request = new RestRequest(wsUri);
            foreach (var p in parameters)
                request.AddParameter(p.Key, p.Value);

            var clnt = new RestClient(baseUrl: Address);
            var result = clnt.Execute(request);
            return result.StatusCode == HttpStatusCode.OK ? result : null;
        }
    }
}
