# Chinook
## Projekt zaliczeniowy na przedmiot Programowanie Rozproszone

### _Paweł Iżycki, Marek Nowicki_

## Koncept 
Projekt miał na celu pokazania przykładu implementacji prostego przykładu opartego o wzorzec [MapReduce](http://en.wikipedia.org/wiki/MapReduce) z wykorzystaniem języków `F#` i `C#`. Jako protokół do komunikacji wykorzystano HTTP. Całość została przystosowana do pracy na platformie [Azure](http://en.wikipedia.org/wiki/Microsoft_Azure).

## Problem
Głównym zadaniem programu jest pobieranie informacji ze stacji pogodowych oraz ich analiza. Ponieważ takich stacji może być bardzo wiele, a komunikacja z każdą może trwać nawet do kilku sekund (zależnie od czasu trwania pomiarów), warto w takim przypadku zastosować zrównoleglenie.

## Jak to działa
Wystarczy wywołać odpowiednią usługę API, np.: http://23.102.4.12:8899/Meteo/Wind/1,2

Dostaniemy odpowiedź w formie JSON z informacjami o wietsze dla podanych w zapytaniu stacji.

![JSON](http://i.imgur.com/08dUaqj.png)


## Implementacja
Ponieważ stacje pogodowe zwracają jedynie surowe pomiary, dalsza ich analiza musi zostać wykonana przez moduł centralny. **Dlatego zastosowaliśmy wzorzec MapReduce**. W fazie **Map** pobierane są informacje ze stacji, a w fazie **Reduce** są poprawiane (odrzucenie najmniejszej i nawiększej wartości) i wyciagana z nich średnia. Użyliśmy do tego języka [F#](http://fsharpforfunandprofit.com/posts/concurrency-intro/), który świetnie sprawdza się w analizie wielkich ilości danych (głównie ze względu na sposób alokacji danych w pamięci).

## API
Moduł centralny komunikuje się z modułami stacji pogodowych za pomocą protokołu HTTP. Każda stacja wystawia proste API, dzięki którym możemy pobrać pomiary (w dowolnej ilości). U nas te pomiary są pseudolosowe, ale można je zastąpić prawdziwymi.

Przykładowe wywołanie webservice'u pobierającego 10 niezależnych pomiarów prędkości wiatru ze stacji o ID = 1.
```
http://localhost:8787/Wind/Direction?stationId=1&sampleSize=10
```

Podobne API wystawia moduł centralny, który pobiera i analizuje dane. Pozwala ono na pobranie prędkości oraz kierunku wiatru z konkretnej stacji. Zwracany jest wówczas JSON.

Wywołanie webservice'u pobierającego dane o wietrze (prędkość, kierunek) ze stacji o ID 1 i 2.

```
http://localhost:8899/Meteo/Wind/1,2
```

Na świat powinien być udostepniony jedynie moduł centralny.

## Azure
Moduł centralny jak i moduły stacji pogodowych pracują jako oddzielne procesy chmurowe, **co pozwala na ich łatwe skalowanie**. Azure pozwala nam na skonfigurowanie platformy sprzętowej tak, aby automatycznie dostosowała się do natężenia ruchu. Na przykład, po przekroczeniu 75% wykorzystania procesora przez moduł centralny, zostanie dołożony kolejny rdzeń, który odciąży pierwszy i zapewni zrównoleglenie odpowiadania na zapytania.

![Azure Cloud Service Dashboard](http://i.imgur.com/hxU0wTv.png)
---
![Azure Cloud Service Scale](http://i.imgur.com/rsVLmdH.png)

Publikacja aplikacji możliwa jest przez portal Azure lub z Visual Studio. Do obu wymagane jest zainstalowanie Azure SDK.

Azure SDK pozwala na lokalne uruchomienie naszej aplikacji wykorzystując emulatory, które imitują platformę Azure.