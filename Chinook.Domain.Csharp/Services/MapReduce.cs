﻿namespace Chinook.Domain.Csharp.Services
{
    public abstract class MapReduce<TData, TMapResult, TReduceResult>
    {
        public TReduceResult Run(TData data)
        {
            var mapResult = Map(data);
            return Reduce(mapResult);
        }

        protected abstract TMapResult Map(TData stations);

        protected abstract TReduceResult Reduce(TMapResult stationWindSpeedMeasures);
    }
}