﻿using System.Collections.Generic;
using System.Linq;
using Chinook.Common.Contracts;
using Chinook.Common.Models;

namespace Chinook.Domain.Csharp.Services.WindMeter
{
    public class WindMeterSrv : IWindMeterService
    {
        private readonly IEnumerable<IWindMeterDevice> _windMeasureDevices;

        public WindMeterSrv(IEnumerable<IWindMeterDevice> windMeasureDevices)
        {
            this._windMeasureDevices = windMeasureDevices;
        }

        private IEnumerable<WindMeasurement> MapReduce(IEnumerable<IWindMeterDevice> meteoStations)
        {
            var windSpeedMapReduce = new WindSpeedMapReduce
            {
                SampleSize = this.SampleSize
            };
            return windSpeedMapReduce.Run(meteoStations)
                                    .Select(x => new WindMeasurement
                                    {
                                        StationId = x.Key,
                                        Speed = x.Value
                                    })
                                    .ToList();
        }

        private IEnumerable<IWindMeterDevice> GetWindMeasureDevices(IEnumerable<int> ids)
        {
            var q = from x in this._windMeasureDevices
                    where ids.Contains(x.Id)
                    select x;
            return q.ToList();
        }

        public int SampleSize { get; set; }
        public IEnumerable<WindMeasurement> GetWindForCities(IEnumerable<int> ids)
        {
            var meteoStations = GetWindMeasureDevices(ids);
            return MapReduce(meteoStations);
        }
    }
}
