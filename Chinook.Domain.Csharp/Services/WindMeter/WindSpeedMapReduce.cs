using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chinook.Common.Contracts;

namespace Chinook.Domain.Csharp.Services.WindMeter
{
    public class WindSpeedMapReduce :
        MapReduce<IEnumerable<IWindMeterDevice>,
            Dictionary<int, IEnumerable<double>>,
            Dictionary<int, double>>
    {
        public int SampleSize { get; set; }

        protected override Dictionary<int, IEnumerable<double>> Map(IEnumerable<IWindMeterDevice> stations)
        {
            var tasks = stations.Select(device => new Task<KeyValuePair<int, IEnumerable<double>>>(() =>
            {
                var windSpeed = device.MeasureWindSpeed(device.Id, SampleSize);
                return new KeyValuePair<int, IEnumerable<double>>(device.Id, windSpeed);
            })).ToList();

            return Task.WhenAll(tasks).Result.ToDictionary(x => x.Key, y => y.Value);
        }

        protected override Dictionary<int, double> Reduce(Dictionary<int, IEnumerable<double>> stationWindSpeedMeasures)
        {
            var tasks = stationWindSpeedMeasures.Select(set => new Task<KeyValuePair<int, double>>(() =>
            {
                var avgWindSpeed = AnalyzeMeasurments(set.Value).Average();
                return new KeyValuePair<int, double>(set.Key, avgWindSpeed);
            })).ToList();

            var result = Task.WhenAll(tasks).Result;
            return result.ToDictionary(x => x.Key, y => y.Value);
        }

        private IEnumerable<double> AnalyzeMeasurments(IEnumerable<double> data)
        {
            // Sort 
            var sortedData = data.ToList();
            sortedData.Sort();

            // Return except first and last item
            return sortedData.GetRange(1, sortedData.Count - 2);
        }
    }
}