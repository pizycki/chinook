﻿namespace Chinook.Common.Models
{
    public class WindMeasurement
    {
        public int StationId { get; set; }
        public double Speed { get; set; }
        public double Direction { get; set; }
    }
}
