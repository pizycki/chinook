﻿using System.Collections.Generic;
using Chinook.Common.Models;

namespace Chinook.Common.Contracts
{
    public interface IWindMeterService
    {
        IEnumerable<WindMeasurement> GetWindForCities(IEnumerable<int> ids);
    }
}
