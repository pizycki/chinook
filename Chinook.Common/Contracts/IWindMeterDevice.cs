﻿using System.Collections.Generic;

namespace Chinook.Common.Contracts
{
    public interface IWindMeterDevice
    {
        int Id { get; }
        IEnumerable<double> MeasureWindSpeed(int stationId, int sampleSize);
        IEnumerable<double> MeasureWindDirection(int stationId, int sampleSize);
    }
}
